import React, {Component} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Home from "../components/Home";
import Login from "../components/Login";
import Cabinet from "../components/Cabinet";
import Header from "../components/Header";
import axios from "axios";
import {STORAGE_NAME} from "../constants";
import Admin from "../components/Admin";
import SignUp from "../components/Login/SignUp";
import Files from "../components/Admin/Files";
import Book from "../components/Cabinet/book";
import Disertation from "../components/Cabinet/disertation";
import Report from "../components/Cabinet/report";
import Journal from "../components/Cabinet/journal";
import Avtoreferat from "../components/Cabinet/avtoreferat";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: null,
            isAuthenticated: false
        };
    }

    getCurrentUser = () => {
        axios.get('/api/user/profile', {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            this.setState({
                currentUser: response.data,
                isAuthenticated: true
            })
        }).catch(reason => {
            this.setState({
                currentUser: null,
                isAuthenticated: false
            })
        }).finally(() => {

        })
    };

    componentDidMount() {
        this.getCurrentUser();
    }

    userLogout=()=>{
        localStorage.removeItem(STORAGE_NAME);
        this.getCurrentUser();
    };


    render() {
        return (
            <Router>
                <div className="container-fluid m-0 p-0 back-rgba">
                            <Header/>
                    <Switch>
                        <Route path="/cabinet" render={(props) =><Cabinet/>}/>
                        <Route exact path="/admin"  render={(props)=><Admin/>}/>
                        <Route exact path="/files"  render={(props)=><Files/>}/>
                        <Route exact path="/books"  render={(props)=><Book/>}/>
                        <Route exact path="/disertations"  render={(props)=><Disertation/>}/>
                        <Route exact path="/reports"  render={(props)=><Report/>}/>
                        <Route exact path="/journals"  render={(props)=><Journal/>}/>
                        <Route exact path="/avtoreferats"  render={(props)=><Avtoreferat/>}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;