import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    Button, ButtonGroup,
    Col,
    Container,
    Form,
    Input,
    InputGroupAddon,
    ListGroup,
    ListGroupItem,
    Pagination, PaginationItem, PaginationLink,
    Row,
    Table
} from "reactstrap";
import axios from "axios";
import {STORAGE_NAME} from "../../constants";
import {NavLink} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCoffee} from "@fortawesome/free-solid-svg-icons/faCoffee";
import "../../index.scss";
import Image from "../../book.jpeg";
import Main from "./main";


class GetAllSources extends Component {

    constructor(props) {
        super(props);
        this.state={
            index:1,
            sources:[],
            apiUrl:'',
            page:1,
            term:'',
            disabled:'disabled'

        }
    }

    onChange =(e)=>{
        e.preventDefault();
        axios.get(`api/get/${this.state.apiUrl}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            },
            params: {
                page:this.state.page,
                term:e.target.value
            }
        }).then(response => {
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    getSources =(value)=>{
        this.state.index === 1 ? value='books': value=value
        this.setState({
            apiUrl:value,
            index:2
        })
        axios.get(`api/get/${value}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            console.log(response.data);
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    pageAble=(e)=>{
        this.setState({
            page:e
        })
        axios.get(`api/get/${this.state.apiUrl}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            },
            params: {
                page:e,
                term:this.state.term
            }
        }).then(response => {
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }



    changeIndex =(inde)=>{
        this.setState({
            index:inde
        })
    }

    componentDidMount() {
      this.getSources();
    }


    render() {
        let iterator = 1;
         const a = this.props.changeIn;
        return (
            <div className="container-fluid">
                <Row className="user-interface">
                    <Col md={3} className=" left-dashboard">
                        <ListGroup className="category">
                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.getSources('books')}>
                                <NavLink exact to="#" >
                                    Books
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.getSources('disertations')}>
                                <NavLink exact to="#">
                                    Disertaions
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.getSources('reports')} >
                                <NavLink exact to="#">
                                    Reports
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.getSources('avtoreferats')}>
                                <NavLink exact to="#">
                                    AvtoReferats
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeIndex('journals')}>
                                <NavLink exact to="#">
                                    Journals
                                </NavLink>
                            </ListGroupItem>

                        </ListGroup>
                    </Col>
                    <Col md={9}>
                        <Row>
                          <Col md={6} className="offset-3 pb-3 mt-3">
                                      <input type="text" className="form-control float-left" id="inputPassword2"
                                             placeholder="search ..." onChange={this.onChange}/>
                          </Col>
                        </Row>
                        <Row>
                            {
                                this.state.sources.map(source=>{
                                    return <Col md={3}>
                                        <div className="book mt-3">
                                            <img src={Image} alt=""/>
                                            <h5 className="text-info text-center mt-3">{iterator++  + ". " + source.name}</h5>
                                        </div>
                                        </Col>
                                })}
                        </Row>
                        <Row>
                            <Col md={9}>

                            </Col>
                            <Col md={3}>
                                <ButtonGroup className="mt-3">
                                    <Button color="primary" onClick={()=>this.pageAble(this.state.page-1)}>Keyinga</Button>
                                    <Button color="success">Oldinga</Button>
                                </ButtonGroup>
                            </Col>
                        </Row>

                    </Col>
                </Row>
                <Main/>
            </div>

        );
    }
}

GetAllSources.propTypes = {};

export default GetAllSources;
