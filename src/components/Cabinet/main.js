import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import Book from '../Image/book.jpeg';
import Disertation from '../Image/disertation.jpg';
import AvtoReferat from '../Image/avtoreferat.png';
import Report from '../Image/хисобот_7.jpg';
import Journal from '../Image/journals.jpg';
import {NavLink} from "react-router-dom";

class Main extends Component {
    render() {
        return (
            <div className="container-fluid main pl-5">
                <div className="rgba">
                    <Row className="mt-3 ml-4">
                        <Col md={4}>
                            <div className="wrap">
                                <NavLink to="/books">
                                    <img src={Book} className="img-fluid img-thumbnail" alt=""/>
                                    <div className="footer">
                                        <div className="rgba"><h5 className="p-2">Kirish</h5></div>
                                        <h5 className='p-2'>Kitoblar</h5>
                                    </div>
                                </NavLink>
                            </div>
                        </Col>

                        <Col md={4}>
                            <div className="wrap">
                                <NavLink to="/disertations">
                                    <img src={Disertation} className="img-fluid img-thumbnail" alt=""/>
                                    <div className="footer">
                                        <div className="rgba"><h5 className="p-2">Kirish</h5></div>
                                        <h5 className='p-2'>Disertatsiyalar</h5>
                                    </div>
                                </NavLink>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="wrap">
                                <NavLink to="/avtoreferats">
                                    <img src={AvtoReferat} className="img-fluid img-thumbnail" alt=""/>
                                    <div className="footer">
                                        <div className="rgba"><h5 className="p-2">Kirish</h5></div>
                                        <h5 className='p-2'>AvtoReferatlar</h5>
                                    </div>
                                </NavLink>
                            </div>
                        </Col>
                    </Row>
                    <Row className="ml-4 mt-2">

                        <Col md={4}>
                            <div className="wrap">
                                <NavLink to="/reports">
                                    <img src={Report} className="img-fluid img-thumbnail" alt=""/>
                                    <div className="footer">
                                        <div className="rgba"><h5 className="p-2">Kirish</h5></div>
                                        <h5 className='p-2'>Atchotlar</h5>
                                    </div>
                                </NavLink>
                            </div>
                        </Col>

                        <Col md={4}>
                            <div className="wrap">
                                <NavLink to="/journals">
                                    <img src={Journal} className="img-fluid img-thumbnail" alt=""/>
                                    <div className="footer">
                                        <div className="rgba"><h5 className="p-2">Kirish</h5></div>
                                        <h5 className='p-2'>Journallar</h5>
                                    </div>
                                </NavLink>
                            </div>
                        </Col>

                        <Col md={4}>
                            <div className="wrap">
                                <NavLink to="/journals">
                                    <img src={Book} className="img-fluid img-thumbnail" alt=""/>
                                    <div className="footer">
                                        <div className="rgba"><h5 className="p-2">Kirish</h5></div>
                                        <h5 className='p-2'>Turli Ma'lumotlar</h5>
                                    </div>
                                </NavLink>

                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

Main.propTypes = {};

export default Main;
