import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import {STORAGE_NAME} from "../../constants";
import {Button, Card, CardBody, CardHeader, Col, Container, Row, Table} from "reactstrap";

class Report extends Component {

    constructor(props) {
        super(props);
        this.state={
            sources:[],
            page:1,
            term:''
        }
    }


    getSources =()=>{
        axios.get(`api/get/reports`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            console.log(response.data);
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }


    onChange =(e)=>{
        e.preventDefault();
        axios.get(`api/get/reports`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            },
            params: {
                page:this.state.page,
                term:e.target.value
            }
        }).then(response => {
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    componentDidMount() {
        this.getSources();
    }

    render() {
        let iterator=1;
        return (
            <div>
                <Container>
                    <Row>
                        <Col md={12}>
                            <Card className="main-body">
                                <CardBody className="pdp">
                                    <Row>
                                        <Col md={6} className="offset-3 pb-3 mt-3">
                                            <input type="text" className="form-control float-left" id="inputPassword2"
                                                   placeholder="search ..." onChange={this.onChange}/>
                                        </Col>
                                    </Row>
                                    <Table >
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Report nomi</th>
                                            <th>Report <b>id</b> nomeri</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        {this.state.sources.map(source=>{
                                            return <tr>
                                                <td>{iterator++}</td>
                                                <td>{source.name}</td>
                                                <td>{source.id_number}</td>
                                                <td><Button className="download" disabled>download</Button></td>
                                            </tr>
                                        })}
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

Report.propTypes = {};

export default Report;

