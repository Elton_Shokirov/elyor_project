import React, {Component} from 'react';
import "../../index.scss";
import Image from "../../logo.jpg";
import Dostav from "../../dostoevskiy.jpg";

class Header extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <div className="header m-0 p-0">
                <h5 className="float-left mr-5"><img src={Image} className="logo  m-3" alt=""/>Seismologiya institut elektron kutubxonasi</h5>
                <h5 className="ml-5">O'qing va O'rganing . Jiddiy kitiblarni o'qing. Qolganini hayot o'zi beradi. <img src={Dostav} className="logo m-3" alt=""/></h5>
            </div>
        );
    }
}

export default Header;