import React, {Component} from 'react';
import "../../index.scss";
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col, FormGroup, Input, Label,
    ListGroup,
    ListGroupItem,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table
} from "reactstrap";
import {NavLink} from "react-router-dom";
import axios from "axios";
import {STORAGE_NAME} from "../../constants";
import {AvField, AvForm} from "availity-reactstrap-validation";

class AddSource extends Component {

    constructor(props) {
        super(props);
        this.state={
            index:1,
            sources:[],
            sourceId:{},
            sourceIdTaken:{},
            sourceNameGetMethod:'books',
            sourceNamePostMethod:'book',
            ID:'',
            modal: false,
            modal1:false,
            modal2:false,
            modal3:false,
            users:[],
            pathTaken:'',
            renderWhich:true,
            takenSources:[],
            begin:0,
            end:0,
            attachmentID:''
        }
    }


    changeToTaken =(value)=>{
        this.setState({
            renderWhich:false,
            pathTaken:value
        })
        axios.get(`api/taken/${value}`,).then(response => {
            console.log(response.data)
            this.setState({
                takenSources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    toggle=()=>{
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    toggle1=()=>{
        this.setState(prevState => ({
            modal1: !prevState.modal1
        }));
    }
    toggle2=()=>{
        this.setState(prevState => ({
            modal2: !prevState.modal2
        }));
    }

    toggle3=()=>{
        this.setState(prevState => ({
            modal3: !prevState.modal3
        }));
    }

    changeIndex =(index, value)=>{
        this.setState({
            renderWhich:true
        })
        switch (index) {
            case 1: this.setState({
                sourceNameGetMethod:'books',
                sourceNamePostMethod:'book'
            });  break;
            case 2: this.setState({
                sourceNameGetMethod:'disertations',
                sourceNamePostMethod:'disertation'
            }); this.getSources(); break;
            case 3: this.setState({
                sourceNameGetMethod:'reports',
                sourceNamePostMethod:'report'
            }); break;
            case 4: this.setState({
                    sourceNameGetMethod:'avtoreferats',
                    sourceNamePostMethod:'avtoreferat'
            }); break;
            case 5:  this.setState({
                sourceNameGetMethod:'journals',
                sourceNamePostMethod:'journal'
            }); break;
        }

        axios.get(`api/get/${value}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    getSources =()=>{
        axios.get(`api/get/${this.state.sourceNameGetMethod}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            console.log(response.data);
                this.setState({
                    sources:response.data.content
                })
        }).catch(reason => {
            console.log(reason);
        });
    }

    handleSubmitSave = (event, errors, values) => {
        console.log(values)
        if (errors.length === 0) {
            axios.post(`/api/save/soure`, values,{
                params:{
                    controller:this.state.sourceNamePostMethod,
                    id:this.state.attachmentID
                }
            }).then(response => {
                console.log(response.data)
                this.getSources();
                }).catch(reason => {
                console.log(reason);
            });
            this.setState({
                modal:!this.state.modal
            })
        }
    };

    handleSubmitEdit = (event, errors, values) => {
        if (errors.length === 0) {
            axios.put(`/api/update/${this.state.sourceNamePostMethod}/${this.state.ID}`, values,{ headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
                }}).then(response => {
                    console.log(response.data);
                this.getSources();
            }).catch(reason => {
                console.log(reason);
            });
            this.setState({
                modal1:!this.state.modal1
            })
        }
    };

    handleSubmitTakenEdit = (event, errors, values) => {
        if (errors.length === 0) {
            axios.put(`/api/taken/update/${this.state.pathTaken}/${this.state.ID}`, values,{ headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
                }}).then(response => {
                console.log(response.data);
                this.takenSourceGet();
            }).catch(reason => {
                console.log(reason);
            });
            this.setState({
                modal1:!this.state.modal1
            })
        }
    };

    delete =(id)=>{
        console.log(id);
        axios.delete(`/api/delete/${this.state.sourceNamePostMethod}/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }}).then(res => {
                  this.getSources();
            }).catch(reason => {
            console.log(reason);
        })

    }

    deleteTaken=(id)=>{
        axios.delete(`api/taken/delete/${this.state.pathTaken}/${id}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            this.takenSourceGet();
        }).catch(reason => {
            console.log(reason);
        });
    }

    getsourceByID = (id) =>{
        axios.get(`api/getId/${this.state.sourceNamePostMethod}/${id}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            this.setState({
                sourceId:response.data,
                modal1: !this.state.modal1,
                ID:id
            })
        }).catch(reason => {
            console.log(reason);
        });

    }

    getsourceByIDTaken = (id) =>{
        axios.get(`api/taken/getId/${this.state.pathTaken}/${id}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            console.log(response.data);
            this.setState({
                sourceIdTaken:response.data,
                modal3: !this.state.modal3,
                ID:id
            })
        }).catch(reason => {
            console.log(reason);
        });

    }



    componentDidMount() {
        this.getSources();
    }

    takenSourceGet =()=>{
        axios.get(`api/taken/${this.state.pathTaken}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }
        }).then(response => {
            console.log(response.data)
            this.setState({
                modal1:false,
                modal2:false,
                modal3:false,
                modal:false,
                takenSources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    handleSubmitSaveTaken = (event, errors, values) => {
        if (errors.length === 0) {
            console.log(values);
            axios.post(`/api/${this.state.pathTaken}`, values,{ headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
                }}).then(response => {
                    this.takenSourceGet();
            }).catch(reason => {
                console.log(reason);
            });
        }
    };


    handleSubmiteExcel = () => {

    };

    begin=(e)=>{
        this.setState({
            begin:e.target.value
        })
    }

    end=(e)=>{
        this.setState({
            end:e.target.value
        })
    }

    onExcel =(e)=>{
        e.preventDefault();
        axios.get(`api/excel/soure`,{
            params: {
                begin:this.state.begin,
                 end:this.state.end
            }
        }).then(response => {
            console.log(response.data)
        }).catch(reason => {
            console.log(reason);
        });
    }

    onChange =(e)=>{
        e.preventDefault();
        axios.get(`api/get/${this.state.sourceNameGetMethod}`,{
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            },
            params: {
                page:this.state.page,
                term:e.target.value
            }
        }).then(response => {
            this.setState({
                sources:response.data.content
            })
        }).catch(reason => {
            console.log(reason);
        });
    }

    // onFileChangeHandler = (e,id) => {
    //     e.preventDefault();
    //     console.log(id);
    //     this.setState({
    //         selectedFile: e.target.files[0]
    //     });
    //     const formData = new FormData();
    //     formData.append('file', e.target.files[0]);
    //     axios.post("api/file", formData, { headers: {
    //             'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
    //         }})
    //         .then(res => {
    //             console.log(res.data);
    //             alert("File uploaded successfully." + res.data);
    //         })
    // };

    onFileChangeHandler = (e) => {
        e.preventDefault();
        this.setState({
            selectedFile: e.target.files[0]
        });
        const formData = new FormData();
        formData.append('file', e.target.files[0])
        axios.post("api/file", formData, { headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }})
            .then(res => {
                console.log(res.data)
                this.setState({
                    attachmentID: res.data.uuid
                })
            })
    };

    render() {
        let iterator = 1;
        let iterator1 = 1;
        const ren = this.state.sources;
        return (
            <div className="container-fluid">
                <Row className="user-interface">
                    <Col md={3} className=" left-dashboard">
                        <ListGroup className="category">
                            <p className="border-info mt-3 text-success">All</p>
                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeIndex(1,"books")}>
                                <NavLink exact to="#" >
                                    Books
                                </NavLink>
                            </ListGroupItem>
                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeIndex(2,"disertations")}>
                                <NavLink exact to="#">
                                    Disertaions
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeIndex(3,"reports")} >
                                <NavLink exact to="#">
                                    Reports
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeIndex(4,"avtoreferats")}>
                                <NavLink exact to="#">
                                    AvtoReferats
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeIndex(5,"journals")}>
                                <NavLink exact to="#">
                                    Journals
                                </NavLink>
                            </ListGroupItem>
                            <p className="text-success mt-3">Taken</p>
                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeToTaken('taken_books')}>
                                <NavLink exact to="#" >
                                    Books
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeToTaken('taken_dissertations')}>
                                <NavLink exact to="#">
                                    Disertaions
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeToTaken('taken_reports')} >
                                <NavLink exact to="#">
                                    Reports
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeToTaken('taken_avtoreferats')}>
                                <NavLink exact to="#">
                                    AvtoReferats
                                </NavLink>
                            </ListGroupItem>

                            <ListGroupItem color="success" className="nav-item" onClick={()=>this.changeToTaken('taken_journals')}>
                                <NavLink exact to="#">
                                    Journals
                                </NavLink>
                            </ListGroupItem>
                        </ListGroup>
                    </Col>
                    <Col md={9}>
                        {this.state.renderWhich ? <Card className="main-body">
                            <CardHeader><Button className="btn-success" onClick={this.toggle}>Add</Button></CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={6} className=" pb-3 mt-3">
                                        <label htmlFor="inputPassword2">Kitob Nomi bo'yicha qidiring</label>
                                        <input type="text" className="form-control float-left" id="inputPassword2"
                                               placeholder="search ..." onChange={this.onChange}/>
                                    </Col>
                                    <Col>
                                        <AvForm>
                                            <AvField name="begin" label="Boshlang'ich yil"  value={this.state.begin} type="text"  required onChange={this.begin} />
                                            <AvField name="end" value={this.state.end} label="Tugash yil" type="text" required onChange={this.end}/>
                                            <a href={'http://localhost:8080/api/excel/soure/?begin=' + this.state.begin + '&end=' + this.state.end + '&check=' + this.state.sourceNamePostMethod}>Yuklash</a>
                                        </AvForm>
                                    </Col>
                                </Row>
                                <Table bordered>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kitob Nomi</th>
                                        <th>Kitob id nomeri</th>
                                        <th>Soni</th>
                                        <th>Kitob yaratilgan vaqti</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    {this.state.sources.map(source=>{
                                        return <tr>
                                            <th>{iterator++}</th>
                                            <th>{source.name}</th>
                                            <th>{source.id_number}</th>
                                            <th>{source.quantity}</th>
                                            <th>{source.created_date}</th>
                                            <th><Button className="float-left mr-2" color="warning" onClick={()=>this.getsourceByID(source.id)}>Edit</Button><Button color="danger" onClick={()=>this.delete(source.id)}>Delete</Button></th>
                                        </tr>
                                    })}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card> : <Card className="main-body m-0 p-0">
                            <CardHeader><Button className="btn-success" onClick={this.toggle2}>Add</Button></CardHeader>
                            <CardBody>
                                <Table bordered>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>F.I.O</th>
                                        <th>Kitob nomi</th>
                                        <th>Kitob id nomeri</th>
                                        <th>Olingan vaqti</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.takenSources.map(source=>{
                                        return <tr>
                                            <th>{iterator1++}</th>
                                            <th>{source.fullName}</th>
                                            <th>{source.soureName}</th>
                                            <th>{source.id_number}</th>
                                            <th>{source.createdAt}</th>
                                            <th><Button className="float-left mr-2" color="warning" onClick={()=>this.getsourceByIDTaken(source.id)}>Edit</Button><Button color="danger" onClick={()=>this.deleteTaken(source.id)}>Delete</Button></th>
                                        </tr>
                                    })}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>}
                    </Col>
                </Row>

                <Row>

                    <Modal isOpen={this.state.modal} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggle}><h3>Add {this.state.sourceNamePostMethod}</h3></ModalHeader>
                        <ModalBody>
                            <AvForm onSubmit={this.handleSubmitSave}>
                                <AvField name="name" label="Name" type="text" required/>
                                <AvField name="id_number" label="Id Number" type="text" required/>
                                <AvField name="quantity" label="Quantity" type="text" required/>
                                <AvField name="created_date" label="Created At" type="text" required/>
                                <div className="form-group files color">
                                    <label>Upload Your File </label>
                                    <input type="file" className="form-control"  name="file" onChange={this.onFileChangeHandler}/>
                                </div>
                                <Button color="primary">Add</Button>
                            </AvForm>
                        </ModalBody>
                        <ModalFooter>
                            <Button className="float-right" color="secondary" onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                    </Modal>


                    <Modal isOpen={this.state.modal1} toggle={this.toggle1} className={this.props.className}>
                        <ModalHeader toggle={this.toggle1}><h3>Edit {this.state.sourceNamePostMethod}</h3></ModalHeader>
                        <ModalBody>
                            <AvForm onSubmit={this.handleSubmitEdit}>
                                <AvField name="name" label="Name" value={this.state.sourceId.name} type="text" required/>
                                <AvField name="id_number" value={this.state.sourceId.id_number} label="Id Number" type="text" required/>
                                <AvField name="quantity" value={this.state.sourceId.quantity} label="Quantity" type="text" required/>
                                <AvField name="createdDate" value={this.state.sourceId.createdDate} label="Quantity" type="text" required/>
                                <Button color="primary">Edit</Button>
                            </AvForm>
                        </ModalBody>
                        <ModalFooter>
                            <Button className="float-right" color="secondary" onClick={this.toggle1}>Cancel</Button>
                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={this.state.modal2} toggle={this.toggle2} className={this.props.className}>
                        <ModalHeader toggle={this.toggle2}><h3>Edit {this.state.sourceNamePostMethod}</h3></ModalHeader>
                        <ModalBody>
                            <AvForm onSubmit={this.handleSubmitSaveTaken}>
                                {/*<AvField type="select" name="firstName" label="Kitob oluvchining ismi :" >*/}
                                {/*    {this.state.users.map(user=>{*/}
                                {/*        return <option value={user.firstName}>{user.firstName}</option>*/}
                                {/*    })}*/}
                                {/*</AvField>*/}
                                {/*<AvField type="select" name="lastName" label="Kitob oluvchining familiyasi :" >*/}
                                {/*    {this.state.users.map(user=>{*/}
                                {/*        return <option value={user.lastName}>{user.lastName}</option>*/}
                                {/*    })}*/}
                                {/*</AvField>*/}
                                {/**/}
                                {/*<AvField type="select" name="username" label="Kitob oluvchining username :" >*/}
                                {/*    {this.state.users.map(user=>{*/}
                                {/*        return <option value={user.username}>{user.username}</option>*/}
                                {/*    })}*/}
                                {/*</AvField>*/}
                                <AvField name="fullName" label="F.I.O" type="text" required/>
                                <AvField name="soureName" label="Kitob nomi" type="text" required/>
                                <AvField name="id_number" label="Kitob id nomeri" type="text" required/>
                                <Button color="primary">Add</Button>
                            </AvForm>
                        </ModalBody>
                        <ModalFooter>
                            <Button className="float-right" color="secondary" onClick={this.toggle2}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.modal3} toggle={this.toggle3} className={this.props.className}>
                        <ModalHeader toggle={this.toggle3}><h3>Edit {this.state.sourceNamePostMethod}</h3></ModalHeader>
                        <ModalBody>
                            <AvForm onSubmit={this.handleSubmitTakenEdit}>
                                {/*<AvField type="select" name="firstName" label="Kitob oluvchining ismi :" >*/}
                                {/*    {this.state.users.map(user=>{*/}
                                {/*        return <option value={user.firstName}>{user.firstName}</option>*/}
                                {/*    })}*/}
                                {/*</AvField>*/}
                                {/*<AvField type="select" name="lastName" label="Kitob oluvchining familiyasi :" >*/}
                                {/*    {this.state.users.map(user=>{*/}
                                {/*        return <option value={user.lastName}>{user.lastName}</option>*/}
                                {/*    })}*/}
                                {/*</AvField>*/}
                                {/**/}
                                {/*<AvField type="select" name="username" label="Kitob oluvchining username :" >*/}
                                {/*    {this.state.users.map(user=>{*/}
                                {/*        return <option value={user.username}>{user.username}</option>*/}
                                {/*    })}*/}
                                {/*</AvField>*/}
                                <AvField name="fullName" value={this.state.sourceIdTaken.fullName} label="F.I.O" type="text" required/>
                                <AvField name="soureName" value={this.state.sourceIdTaken.soureName}  label="Kitob nomi" type="text" required/>
                                <AvField name="id_number"  value={this.state.sourceIdTaken.id_number} label="Kitob id nomeri" type="text" required/>
                                <Button color="primary">Add</Button>
                            </AvForm>
                        </ModalBody>
                        <ModalFooter>
                            <Button className="float-right" color="secondary" onClick={this.toggle2}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </Row>
            </div>
        );
    }
}

AddSource.propTypes = {};

export default AddSource;
