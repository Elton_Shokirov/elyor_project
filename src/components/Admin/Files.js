import React, {Component} from 'react';
import axios from "axios";
import {STORAGE_NAME} from "../../constants";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";

class Files extends Component {
    constructor(props) {
        super(props);
        this.state={
            selectedFile:''
        }
    }


    onFileChangeHandler = (e) => {
        e.preventDefault();
        this.setState({
            selectedFile: e.target.files[0]
        });
        const formData = new FormData();
        formData.append('file', e.target.files[0])
        axios.post("api/file", formData, { headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(STORAGE_NAME)
            }})
            .then(res => {
                console.log(res.data);
                alert("File uploaded successfully." + res.data);
            })
    };

    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group files color">
                            <label>Upload Your File </label>
                            <input type="file" className="form-control" name="file" onChange={this.onFileChangeHandler}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Files.propTypes = {};

export default Files;
