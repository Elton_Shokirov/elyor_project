import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, CardBody, CardHeader, Col, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {NavLink, Redirect, Route, Switch} from "react-router-dom";
import axios from "axios";
import {STORAGE_NAME} from "../../constants";
import App from "../../layouts/App";
import Login from "./index";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state={
            checkLayoult:false
        }

    }

    handleSubmit = (event, errors, values) => {
        if (errors.length === 0) {
            axios.post(`/api/auth/register`, values).then(response => {
                localStorage.setItem(STORAGE_NAME, response.data.accessToken);
                this.setState({
                    checkLayoult:true
                })

            }).catch(reason => {
                console.log(reason);
            });
        }
    };

    render() {
        return (
            <div>
                <Row>
                    <Col md={6} className="offset-3">
                        <Card>
                            <CardBody>
                                <AvForm onSubmit={this.handleSubmit}>
                                    <AvField name="firstName" label="First Name" type="text" required/>
                                    <AvField name="lastName" label="Last Name" type="text" required/>
                                    <AvField name="phoneNumber" label="Phone Number" type="text" required/>
                                    <AvField name="username" label="Username" type="text" required/>
                                    <AvField name="password" label="Password" type="password" required/>
                                    <Button color="primary">Login</Button>
                                </AvForm>
                            </CardBody>
                            <CardHeader>
                                username va parolingizni esdan chiqarmang
                            </CardHeader>
                        </Card>
                    </Col>
                </Row>

            </div>
        );
    }
}

SignUp.propTypes = {};

export default SignUp;
