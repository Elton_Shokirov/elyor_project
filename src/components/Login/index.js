import React, {Component} from 'react';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import {Button, Card, CardBody, CardHeader} from "reactstrap";
import './index.css';
import axios from "axios";
import {STORAGE_NAME} from "../../constants";
import {NavLink} from "react-router-dom";
class Login extends Component {

    constructor(props) {
        super(props);
    }

    handleSubmit = (event, errors, values) => {
        if (errors.length === 0) {
            axios.post("/api/auth/login", values)
                .then(response => {
                   localStorage.setItem(STORAGE_NAME, response.data.accessToken);
                   this.props.onLogin();
                }).catch(reason => {
                    console.log(reason);
            });
        }
    };

    render() {
        return (
            <div className="login">
                <Card>
                    <CardHeader>
                       <h6> username va parolingizni kiriting.</h6>
                    </CardHeader>
                    <CardBody>
                        <AvForm onSubmit={this.handleSubmit}>
                            <AvField name="username" label="Username" type="text" required/>
                            <AvField name="password" label="Password" type="password" required/>
                            <Button  color="primary">Login</Button>
                        </AvForm>
                    </CardBody>
                    <CardHeader>
                        registratsiya uchun <NavLink to='/signup'> register </NavLink> ni bosing
                    </CardHeader>
                </Card>
            </div>
        );
    }
}

export default Login;