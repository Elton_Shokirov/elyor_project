import React, {Component} from 'react';

import {Col, Container, Row} from "reactstrap";
import GetAllSources from "../Cabinet/getAllSources";


class Home extends Component {

    render() {
        return (
            <div>
                <GetAllSources/>
            </div>
        );
    }
}

export default Home;